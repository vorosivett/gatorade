EESchema Schematic File Version 4
LIBS:Gatorade-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L device:LED D6
U 1 1 5D987FCB
P 9650 3200
F 0 "D6" V 9688 3083 50  0000 R CNN
F 1 "LED" V 9597 3083 50  0000 R CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 9650 3200 50  0001 C CNN
F 3 "~" H 9650 3200 50  0001 C CNN
	1    9650 3200
	1    0    0    -1  
$EndComp
Text GLabel 10250 3200 2    50   Input ~ 0
+5V
Wire Wire Line
	9800 3200 9850 3200
Wire Wire Line
	10250 3200 10150 3200
$Comp
L MCU_Microchip_PIC16:PIC16(L)F1454-I_P U2
U 1 1 5D98B811
P 3350 2350
F 0 "U2" H 3350 3128 50  0000 C CNN
F 1 "PIC16(L)F1454-I_P" H 3350 3037 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 3350 2350 50  0001 C CNN
F 3 "https://hu.farnell.com/microchip/pic16f15324-i-sl/mcu-8bit-pic16-32mhz-soic-14/dp/2857680" H 3350 2350 50  0001 C CNN
	1    3350 2350
	1    0    0    -1  
$EndComp
$Comp
L device:LED D1
U 1 1 5D98CDAA
P 9650 1450
F 0 "D1" V 9688 1333 50  0000 R CNN
F 1 "LED" V 9597 1333 50  0000 R CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 9650 1450 50  0001 C CNN
F 3 "~" H 9650 1450 50  0001 C CNN
	1    9650 1450
	1    0    0    -1  
$EndComp
$Comp
L device:R R1
U 1 1 5D98CDB2
P 10000 1450
F 0 "R1" H 9930 1404 50  0000 R CNN
F 1 "500" H 9930 1495 50  0000 R CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9930 1450 50  0001 C CNN
F 3 "" H 10000 1450 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x4990ftl/res-499r-1-0-1w-0603-thick-film/dp/2694886" H 10000 1450 50  0001 C CNN "link"
	1    10000 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9800 1450 9850 1450
Text GLabel 7500 2700 3    50   Input ~ 0
GND
Wire Wire Line
	10250 1450 10150 1450
$Comp
L device:LED D4
U 1 1 5D98D399
P 9650 2500
F 0 "D4" V 9688 2383 50  0000 R CNN
F 1 "LED" V 9597 2383 50  0000 R CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 9650 2500 50  0001 C CNN
F 3 "~" H 9650 2500 50  0001 C CNN
	1    9650 2500
	1    0    0    -1  
$EndComp
Text GLabel 10250 2500 2    50   Input ~ 0
+5V
Wire Wire Line
	9800 2500 9850 2500
Wire Wire Line
	10250 2500 10150 2500
$Comp
L device:LED D5
U 1 1 5D98D3BD
P 9650 2850
F 0 "D5" V 9688 2733 50  0000 R CNN
F 1 "LED" V 9597 2733 50  0000 R CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 9650 2850 50  0001 C CNN
F 3 "~" H 9650 2850 50  0001 C CNN
	1    9650 2850
	1    0    0    -1  
$EndComp
Text GLabel 10250 2850 2    50   Input ~ 0
+5V
Wire Wire Line
	9800 2850 9850 2850
Wire Wire Line
	10250 2850 10150 2850
$Comp
L device:LED D2
U 1 1 5D98DD08
P 9650 1800
F 0 "D2" V 9688 1683 50  0000 R CNN
F 1 "LED" V 9597 1683 50  0000 R CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 9650 1800 50  0001 C CNN
F 3 "~" H 9650 1800 50  0001 C CNN
	1    9650 1800
	1    0    0    -1  
$EndComp
Text GLabel 10250 1800 2    50   Input ~ 0
+5V
Wire Wire Line
	9800 1800 9850 1800
Wire Wire Line
	10250 1800 10150 1800
$Comp
L device:LED D3
U 1 1 5D98DD2C
P 9650 2150
F 0 "D3" V 9688 2033 50  0000 R CNN
F 1 "LED" V 9597 2033 50  0000 R CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 9650 2150 50  0001 C CNN
F 3 "~" H 9650 2150 50  0001 C CNN
	1    9650 2150
	1    0    0    -1  
$EndComp
Text GLabel 10250 2150 2    50   Input ~ 0
+5V
Wire Wire Line
	9800 2150 9850 2150
Wire Wire Line
	10250 2150 10150 2150
$Comp
L conn:CONN_01X05 P5
U 1 1 5D9900E3
P 1200 7300
F 0 "P5" H 1278 7341 50  0000 L CNN
F 1 "CONN_01X05" H 1278 7250 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x05_Pitch2.54mm" H 1200 7300 60  0001 C CNN
F 3 "" H 1200 7300 60  0000 C CNN
	1    1200 7300
	1    0    0    -1  
$EndComp
Text GLabel 900  7100 0    50   Input ~ 0
MCLR
Text GLabel 900  7200 0    50   Input ~ 0
+5V
Text GLabel 900  7300 0    50   Input ~ 0
GND
Text GLabel 900  7400 0    50   Input ~ 0
PGD
Text GLabel 900  7500 0    50   Input ~ 0
PGC
Wire Wire Line
	900  7500 1000 7500
Wire Wire Line
	1000 7400 900  7400
Wire Wire Line
	900  7300 1000 7300
Wire Wire Line
	1000 7200 900  7200
Wire Wire Line
	900  7100 1000 7100
Text GLabel 1350 2450 0    50   Input ~ 0
MCLR
Text GLabel 1350 2250 0    50   Input ~ 0
PGD
Text GLabel 850  2200 0    50   Input ~ 0
PGC
Text GLabel 3350 3200 3    50   Input ~ 0
GND
Wire Wire Line
	3350 3200 3350 2950
Wire Wire Line
	3350 900  3350 1000
Text GLabel 2600 1400 0    50   Input ~ 0
GND
$Comp
L device:C C?
U 1 1 5D9A8448
P 2950 1400
AR Path="/5D9A66DA/5D9A8448" Ref="C?"  Part="1" 
AR Path="/5D9A8448" Ref="C1"  Part="1" 
F 0 "C1" V 2698 1400 50  0000 C CNN
F 1 "10μF" V 2789 1400 50  0000 C CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2988 1250 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2340522.pdf" H 2950 1400 50  0001 C CNN
F 4 "https://hu.farnell.com/samsung-electro-mechanics/cl10a106mq8nnnc/cap-10uf-6-3v-mlcc-0603/dp/3013391" V 2950 1400 50  0001 C CNN "link"
	1    2950 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 1400 3800 1400
Text GLabel 4000 1400 2    50   Input ~ 0
GND
Wire Wire Line
	3350 1400 3350 1200
Connection ~ 3350 1400
Wire Wire Line
	3500 1400 3350 1400
$Comp
L device:C C?
U 1 1 5D9A8455
P 3650 1400
AR Path="/5D9A66DA/5D9A8455" Ref="C?"  Part="1" 
AR Path="/5D9A8455" Ref="C2"  Part="1" 
F 0 "C2" V 3902 1400 50  0000 C CNN
F 1 "100nF" V 3811 1400 50  0000 C CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3688 1250 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2079171.pdf" H 3650 1400 50  0001 C CNN
F 4 "https://hu.farnell.com/kemet/c0603c104m4ractu/cap-0-1-f-16v-20-x7r-0603/dp/2581045" V 3650 1400 50  0001 C CNN "link"
	1    3650 1400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3350 1750 3350 1400
Text GLabel 3350 900  1    50   Input ~ 0
+5V
$Comp
L device:Jumper_NO_Small JP?
U 1 1 5D9A845E
P 3350 1100
AR Path="/5D9A66DA/5D9A845E" Ref="JP?"  Part="1" 
AR Path="/5D9A845E" Ref="JP1"  Part="1" 
F 0 "JP1" H 3350 1285 50  0000 C CNN
F 1 "JMP" H 3350 1194 50  0000 C CNN
F 2 "jumper:SOLDER-JUMPER_1-WAY" H 3350 1100 50  0001 C CNN
F 3 "~" H 3350 1100 50  0001 C CNN
	1    3350 1100
	0    1    1    0   
$EndComp
$Comp
L conn:CONN_01X01 P1
U 1 1 5D9B417F
P 8400 2600
F 0 "P1" H 8477 2694 50  0000 L CNN
F 1 "CONN_01X01" H 8477 2603 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01_Pitch2.54mm" H 8477 2504 60  0001 L CNN
F 3 "" H 8400 2600 60  0000 C CNN
	1    8400 2600
	0    1    1    0   
$EndComp
Text GLabel 1550 4950 0    50   Input ~ 0
+5V
$Comp
L device:R R10
U 1 1 5D9BD7D7
P 1550 5300
F 0 "R10" V 1757 5300 50  0000 C CNN
F 1 "10k" V 1666 5300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1480 5300 50  0001 C CNN
F 3 "" H 1550 5300 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x1002ftl/res-10k-1-0-1w-0603-thick-film/dp/2447230" V 1550 5300 50  0001 C CNN "link"
	1    1550 5300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1700 5300 1950 5300
Wire Wire Line
	1950 5300 1950 5250
Wire Wire Line
	1550 4950 1750 4950
$Comp
L conn:CONN_01X01 P2
U 1 1 5D9BD7E1
P 950 5300
F 0 "P2" H 869 4969 50  0000 C CNN
F 1 "CONN_01X01" H 869 5060 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01_Pitch2.54mm" H 869 5159 60  0001 C CNN
F 3 "" H 950 5300 60  0000 C CNN
	1    950  5300
	-1   0    0    1   
$EndComp
Wire Wire Line
	2900 4800 2900 4950
Text GLabel 2900 4300 1    50   Input ~ 0
GND
Wire Wire Line
	2900 4300 2900 4500
Wire Wire Line
	1150 5300 1400 5300
Text GLabel 6950 2150 0    50   Input ~ 0
OUT
Text GLabel 3000 4950 2    50   Input ~ 0
IN0
$Comp
L device:R R9
U 1 1 5D9C3323
P 2700 4950
F 0 "R9" V 2907 4950 50  0000 C CNN
F 1 "2k" V 2816 4950 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 2630 4950 50  0001 C CNN
F 3 "" H 2700 4950 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x2001ftl/res-2k-1-0-1w-0603-thick-film/dp/2447319" V 2700 4950 50  0001 C CNN "link"
	1    2700 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	2850 4950 2900 4950
Text GLabel 1550 6100 0    50   Input ~ 0
+5V
Wire Wire Line
	1700 6450 1950 6450
Wire Wire Line
	1950 6450 1950 6400
Wire Wire Line
	1550 6100 1750 6100
$Comp
L conn:CONN_01X01 P3
U 1 1 5D9CFB97
P 950 6450
F 0 "P3" H 869 6225 50  0000 C CNN
F 1 "CONN_01X01" H 869 6316 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01_Pitch2.54mm" H 869 6309 60  0001 C CNN
F 3 "" H 950 6450 60  0000 C CNN
	1    950  6450
	-1   0    0    1   
$EndComp
Wire Wire Line
	2900 5950 2900 6100
Text GLabel 2900 5450 1    50   Input ~ 0
GND
Wire Wire Line
	2900 5450 2900 5650
Wire Wire Line
	1150 6450 1400 6450
Text GLabel 3000 6100 2    50   Input ~ 0
IN1
Wire Wire Line
	2850 6100 2900 6100
$Comp
L device:Jumper_NO_Small JP2
U 1 1 5D9D263F
P 5700 1850
F 0 "JP2" H 5700 1943 50  0000 C CNN
F 1 "Jumper_NO_Small" H 5700 1944 50  0001 C CNN
F 2 "jumper:SOLDER-JUMPER_1-WAY" H 5700 1850 50  0001 C CNN
F 3 "" H 5700 1850 50  0001 C CNN
	1    5700 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 2250 5600 2250
Wire Wire Line
	5350 2150 5500 2150
Wire Wire Line
	5500 2150 5500 2050
Wire Wire Line
	5500 2050 5600 2050
Wire Wire Line
	5350 2050 5450 2050
Wire Wire Line
	5450 2050 5450 1850
Wire Wire Line
	5450 1850 5600 1850
Text GLabel 5900 1850 2    50   Input ~ 0
OUT
Text GLabel 5900 2050 2    50   Input ~ 0
IN0
Text GLabel 5900 2250 2    50   Input ~ 0
IN1
Wire Wire Line
	5800 1850 5900 1850
Wire Wire Line
	5800 2050 5900 2050
Wire Wire Line
	5800 2250 5900 2250
Text GLabel 6950 1550 0    50   Input ~ 0
LED0
Text GLabel 6950 1650 0    50   Input ~ 0
LED1
Text GLabel 6950 1750 0    50   Input ~ 0
LED2
Text GLabel 6950 1850 0    50   Input ~ 0
LED3
Text GLabel 6950 1950 0    50   Input ~ 0
LED4
Text GLabel 6950 2050 0    50   Input ~ 0
LED5
Text GLabel 5400 2350 2    50   Input ~ 0
LED0
Text GLabel 5400 2450 2    50   Input ~ 0
LED1
Text GLabel 5400 2550 2    50   Input ~ 0
LED2
Wire Wire Line
	5350 2350 5400 2350
Wire Wire Line
	5350 2450 5400 2450
Wire Wire Line
	5350 2550 5400 2550
Text GLabel 1350 2650 0    50   Input ~ 0
LED3
Text GLabel 1350 2550 0    50   Input ~ 0
LED4
Text GLabel 850  2350 0    50   Input ~ 0
LED5
Wire Wire Line
	1350 2350 1000 2350
Wire Wire Line
	850  2200 1000 2200
Wire Wire Line
	1000 2200 1000 2350
Connection ~ 1000 2350
Wire Wire Line
	1000 2350 850  2350
$Comp
L conn:CONN_01X02 P4
U 1 1 5DA09560
P 2450 7150
F 0 "P4" H 2528 7191 50  0000 L CNN
F 1 "CONN_01X02" H 2528 7100 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x02_Pitch2.54mm" H 2527 7054 60  0001 L CNN
F 3 "" H 2450 7150 60  0000 C CNN
	1    2450 7150
	1    0    0    -1  
$EndComp
Text GLabel 2100 7200 0    50   Input ~ 0
GND
Wire Wire Line
	2100 7200 2250 7200
Text GLabel 2100 7100 0    50   Input ~ 0
+5V
Wire Wire Line
	2100 7100 2250 7100
$Comp
L device:Jumper_NO_Small JP3
U 1 1 5DA12379
P 5700 2050
F 0 "JP3" H 5700 2143 50  0000 C CNN
F 1 "Jumper_NO_Small" H 5700 2144 50  0001 C CNN
F 2 "jumper:SOLDER-JUMPER_1-WAY" H 5700 2050 50  0001 C CNN
F 3 "" H 5700 2050 50  0001 C CNN
	1    5700 2050
	1    0    0    -1  
$EndComp
$Comp
L device:Jumper_NO_Small JP4
U 1 1 5DA123EB
P 5700 2250
F 0 "JP4" H 5700 2343 50  0000 C CNN
F 1 "Jumper_NO_Small" H 5700 2344 50  0001 C CNN
F 2 "jumper:SOLDER-JUMPER_1-WAY" H 5700 2250 50  0001 C CNN
F 3 "" H 5700 2250 50  0001 C CNN
	1    5700 2250
	1    0    0    -1  
$EndComp
NoConn ~ 1350 2050
Wire Wire Line
	3100 1400 3350 1400
Wire Wire Line
	2800 1400 2600 1400
Connection ~ 2900 4950
Wire Wire Line
	2900 4950 3000 4950
Wire Wire Line
	2150 4950 2550 4950
Connection ~ 2900 6100
Wire Wire Line
	2900 6100 3000 6100
Wire Wire Line
	2150 6100 2550 6100
$Comp
L interface:ULN2004 U1
U 1 1 5D9A809D
P 7500 1850
F 0 "U1" H 7500 2417 50  0000 C CNN
F 1 "ULN2004" H 7500 2326 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 7550 1200 50  0001 L CNN
F 3 "https://hu.farnell.com/stmicroelectronics/uln2004d1013tr/transistor-array-7-npn-16soic/dp/1564361?st=uln2004" H 7600 1750 50  0001 C CNN
	1    7500 1850
	1    0    0    -1  
$EndComp
$Comp
L device:Q_DUAL_NPN_NPN_E1B1C2E2B2C1 Q1
U 1 1 5D9A0E69
P 1950 6200
F 0 "Q1" V 2186 6200 50  0000 C CNN
F 1 "Q_DUAL_NPN_NPN_E1B1C2E2B2C1" H 2141 6155 50  0001 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-363_SC-70-6_Handsoldering" H 2150 6300 50  0001 C CNN
F 3 "https://hu.farnell.com/on-semiconductor/mbt3904dw1t1g/bipolar-transistor/dp/1653615" H 1950 6200 50  0001 C CNN
	1    1950 6200
	0    -1   -1   0   
$EndComp
$Comp
L device:Q_DUAL_NPN_NPN_E1B1C2E2B2C1 Q1
U 2 1 5D9A1054
P 1950 5050
F 0 "Q1" V 2186 5050 50  0000 C CNN
F 1 "Q_DUAL_NPN_NPN_E1B1C2E2B2C1" H 2141 5005 50  0001 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-363_SC-70-6_Handsoldering" H 2150 5150 50  0001 C CNN
F 3 "https://hu.farnell.com/on-semiconductor/mbt3904dw1t1g/bipolar-transistor/dp/1653615" H 1950 5050 50  0001 C CNN
F 4 "https://hu.farnell.com/on-semiconductor/mbt3904dw1t1g/bipolar-transistor/dp/1653615" V 1950 5050 50  0001 C CNN "link"
	2    1950 5050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7500 2550 7500 2700
Text GLabel 10250 1450 2    50   Input ~ 0
+5V
Wire Wire Line
	6950 1550 7100 1550
Wire Wire Line
	6950 1650 7100 1650
Wire Wire Line
	7100 1750 6950 1750
Wire Wire Line
	6950 1850 7100 1850
Wire Wire Line
	7100 1950 6950 1950
Wire Wire Line
	6950 2050 7100 2050
Wire Wire Line
	9050 1750 9050 2150
Wire Wire Line
	9050 2150 9500 2150
Wire Wire Line
	8950 1850 8950 2500
Wire Wire Line
	8950 2500 9500 2500
Wire Wire Line
	9500 2850 8850 2850
Wire Wire Line
	8850 2850 8850 1950
Wire Wire Line
	8750 2050 8750 3200
Wire Wire Line
	8750 3200 9500 3200
Wire Wire Line
	9150 1650 9150 1800
Wire Wire Line
	9150 1800 9500 1800
Wire Wire Line
	9250 1550 9250 1450
Wire Wire Line
	9250 1450 9500 1450
Text GLabel 8150 2500 3    50   Input ~ 0
+5V
$Comp
L device:Jumper_NO_Small JP5
U 1 1 5DA2B897
P 8000 2350
F 0 "JP5" H 8000 2443 50  0000 C CNN
F 1 "Jumper_NO_Small" H 8000 2444 50  0001 C CNN
F 2 "jumper:SOLDER-JUMPER_1-WAY" H 8000 2350 50  0001 C CNN
F 3 "" H 8000 2350 50  0001 C CNN
	1    8000 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 2350 8150 2500
Wire Wire Line
	8150 2350 8100 2350
Text Notes 850  4250 0    197  ~ 0
INPUTS
Text Notes 6750 1050 0    197  ~ 0
INDICATORS
Wire Wire Line
	6950 2150 7100 2150
$Comp
L device:R R3
U 1 1 5DA41BAF
P 8200 2150
F 0 "R3" H 8130 2104 50  0000 R CNN
F 1 "50" H 8130 2195 50  0000 R CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 8130 2150 50  0001 C CNN
F 3 "" H 8200 2150 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x49r9ftl/res-49r9-1-0-1w-0603-thick-film/dp/2447381" H 8200 2150 50  0001 C CNN "link"
	1    8200 2150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8350 2150 8400 2150
Wire Wire Line
	8400 2150 8400 2400
Wire Wire Line
	7900 2150 8050 2150
Wire Wire Line
	7900 1550 9250 1550
Wire Wire Line
	7900 1650 9150 1650
Wire Wire Line
	7900 1750 9050 1750
Wire Wire Line
	7900 1850 8950 1850
Wire Wire Line
	7900 1950 8850 1950
Wire Wire Line
	7900 2050 8750 2050
$Comp
L device:R R12
U 1 1 5DA5CEAF
P 2700 6100
F 0 "R12" V 2907 6100 50  0000 C CNN
F 1 "2k" V 2816 6100 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 2630 6100 50  0001 C CNN
F 3 "" H 2700 6100 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x2001ftl/res-2k-1-0-1w-0603-thick-film/dp/2447319" V 2700 6100 50  0001 C CNN "link"
	1    2700 6100
	0    1    1    0   
$EndComp
$Comp
L device:R R2
U 1 1 5DA5D49D
P 10000 1800
F 0 "R2" H 9930 1754 50  0000 R CNN
F 1 "500" H 9930 1845 50  0000 R CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9930 1800 50  0001 C CNN
F 3 "" H 10000 1800 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x4990ftl/res-499r-1-0-1w-0603-thick-film/dp/2694886" H 10000 1800 50  0001 C CNN "link"
	1    10000 1800
	0    -1   -1   0   
$EndComp
$Comp
L device:R R4
U 1 1 5DA5D4F7
P 10000 2150
F 0 "R4" H 9930 2104 50  0000 R CNN
F 1 "500" H 9930 2195 50  0000 R CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9930 2150 50  0001 C CNN
F 3 "" H 10000 2150 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x4990ftl/res-499r-1-0-1w-0603-thick-film/dp/2694886" H 10000 2150 50  0001 C CNN "link"
	1    10000 2150
	0    -1   -1   0   
$EndComp
$Comp
L device:R R5
U 1 1 5DA5D553
P 10000 2500
F 0 "R5" H 9930 2454 50  0000 R CNN
F 1 "500" H 9930 2545 50  0000 R CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9930 2500 50  0001 C CNN
F 3 "" H 10000 2500 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x4990ftl/res-499r-1-0-1w-0603-thick-film/dp/2694886" H 10000 2500 50  0001 C CNN "link"
	1    10000 2500
	0    -1   -1   0   
$EndComp
$Comp
L device:R R6
U 1 1 5DA5D5B1
P 10000 2850
F 0 "R6" H 9930 2804 50  0000 R CNN
F 1 "500" H 9930 2895 50  0000 R CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9930 2850 50  0001 C CNN
F 3 "" H 10000 2850 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x4990ftl/res-499r-1-0-1w-0603-thick-film/dp/2694886" H 10000 2850 50  0001 C CNN "link"
	1    10000 2850
	0    -1   -1   0   
$EndComp
$Comp
L device:R R7
U 1 1 5DA5D611
P 10000 3200
F 0 "R7" H 9930 3154 50  0000 R CNN
F 1 "500" H 9930 3245 50  0000 R CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9930 3200 50  0001 C CNN
F 3 "" H 10000 3200 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x4990ftl/res-499r-1-0-1w-0603-thick-film/dp/2694886" H 10000 3200 50  0001 C CNN "link"
	1    10000 3200
	0    -1   -1   0   
$EndComp
$Comp
L device:R R13
U 1 1 5DA5DBAD
P 1550 6450
F 0 "R13" V 1757 6450 50  0000 C CNN
F 1 "10k" V 1666 6450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1480 6450 50  0001 C CNN
F 3 "" H 1550 6450 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x1002ftl/res-10k-1-0-1w-0603-thick-film/dp/2447230" V 1550 6450 50  0001 C CNN "link"
	1    1550 6450
	0    -1   -1   0   
$EndComp
$Comp
L device:R R8
U 1 1 5DA5DC29
P 2900 4650
F 0 "R8" V 3107 4650 50  0000 C CNN
F 1 "10k" V 3016 4650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 2830 4650 50  0001 C CNN
F 3 "" H 2900 4650 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x1002ftl/res-10k-1-0-1w-0603-thick-film/dp/2447230" V 2900 4650 50  0001 C CNN "link"
	1    2900 4650
	1    0    0    -1  
$EndComp
$Comp
L device:R R11
U 1 1 5DA5DD0F
P 2900 5800
F 0 "R11" V 3107 5800 50  0000 C CNN
F 1 "10k" V 3016 5800 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 2830 5800 50  0001 C CNN
F 3 "" H 2900 5800 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x1002ftl/res-10k-1-0-1w-0603-thick-film/dp/2447230" V 2900 5800 50  0001 C CNN "link"
	1    2900 5800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
