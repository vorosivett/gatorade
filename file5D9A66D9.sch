EESchema Schematic File Version 4
LIBS:Gatorade-cache
LIBS:RelayTester-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D95CFBF
P 9350 2400
F 0 "R?" H 9420 2446 50  0000 L CNN
F 1 "200Ω" H 9420 2355 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9280 2400 50  0001 C CNN
F 3 "~" H 9350 2400 50  0001 C CNN
F 4 "https://hu.farnell.com/panasonic/erj3ekf2000v/res-200r-1-0-1w-0603-thick-film/dp/2059293" H 9350 2400 50  0001 C CNN "link"
	1    9350 2400
	0    1    1    0   
$EndComp
$Comp
L RelayTester-rescue:Jumper_NO_Small-Device JP?
U 1 1 5D95F6CF
P 8550 2050
F 0 "JP?" H 8550 2235 50  0000 C CNN
F 1 "JMP" H 8550 2144 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 8550 2050 50  0001 C CNN
F 3 "~" H 8550 2050 50  0001 C CNN
	1    8550 2050
	1    0    0    -1  
$EndComp
$Comp
L RelayTester-rescue:BSP129-Transistor_FET Q?
U 1 1 5D95E209
P 9900 2400
F 0 "Q?" H 10106 2446 50  0000 L CNN
F 1 "BSP129" H 10106 2355 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 10100 2325 50  0001 L CIN
F 3 "http://www.farnell.com/datasheets/1932422.pdf" H 9900 2400 50  0001 L CNN
F 4 "https://hu.farnell.com/infineon/bsp129h6906xtsa1/mosfet-n-ch-aec-q101-240v-0-35a/dp/2839436?st=bsp129" H 9900 2400 50  0001 C CNN "link"
	1    9900 2400
	1    0    0    -1  
$EndComp
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D962542
P 9000 2850
F 0 "R?" H 9070 2896 50  0000 L CNN
F 1 "10kΩ" H 9070 2805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8930 2850 50  0001 C CNN
F 3 "~" H 9000 2850 50  0001 C CNN
F 4 "https://hu.farnell.com/vishay/crcw060310k0jnea/res-10k-5-0-1w-0603-thick-film/dp/1469749" H 9000 2850 50  0001 C CNN "link"
	1    9000 2850
	-1   0    0    1   
$EndComp
Wire Wire Line
	8250 3000 8250 3250
Wire Wire Line
	8250 3250 9000 3250
Wire Wire Line
	9000 3250 9000 3000
Wire Wire Line
	9000 2700 9000 2400
Wire Wire Line
	9000 2400 8250 2400
Wire Wire Line
	8250 2400 8250 2700
Wire Wire Line
	9000 2400 9200 2400
Connection ~ 9000 2400
Wire Wire Line
	9500 2400 9700 2400
Text GLabel 10000 1250 1    50   Input ~ 0
+5V
$Comp
L Connector:Conn_01x02_Male J?
U 1 1 5D967C39
P 10200 1650
F 0 "J?" H 10172 1532 50  0000 R CNN
F 1 "Conn_01x02_Male" H 10172 1623 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 10200 1650 50  0001 C CNN
F 3 "~" H 10200 1650 50  0001 C CNN
	1    10200 1650
	-1   0    0    1   
$EndComp
Wire Wire Line
	10000 1550 10000 1250
Text GLabel 10000 2900 3    50   Input ~ 0
GND
Wire Wire Line
	10000 2900 10000 2600
Text Notes 10450 1500 0    50   ~ 0
Relay Coil
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5D970303
P 4600 6950
F 0 "J?" V 4662 7094 50  0000 L CNN
F 1 "Conn_01x03_Male" V 4753 7094 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x03_P5.715mm_Drill2mm" H 4600 6950 50  0001 C CNN
F 3 "~" H 4600 6950 50  0001 C CNN
	1    4600 6950
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5D9708A7
P 5900 6900
F 0 "J?" V 5962 7044 50  0000 L CNN
F 1 "Conn_01x03_Male" V 6053 7044 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x03_P5.715mm_Drill2mm" H 5900 6900 50  0001 C CNN
F 3 "~" H 5900 6900 50  0001 C CNN
	1    5900 6900
	0    1    1    0   
$EndComp
Text Notes 4500 6800 0    50   ~ 0
Relay\nCoil Connector\nA
Text GLabel 4050 7350 0    50   Input ~ 0
GND
Wire Wire Line
	4500 7150 4500 7350
Wire Wire Line
	4500 7350 4050 7350
Wire Wire Line
	4500 7350 4600 7350
Wire Wire Line
	4600 7350 4600 7150
Connection ~ 4500 7350
Wire Wire Line
	4600 7350 4700 7350
Wire Wire Line
	4700 7350 4700 7150
Connection ~ 4600 7350
$Comp
L RelayTester-rescue:TLP291-Isolator U?
U 1 1 5D97B4CE
P 1550 1850
F 0 "U?" H 1550 2175 50  0000 C CNN
F 1 "TLP291" H 1550 2084 50  0000 C CNN
F 2 "Package_SO:SOIC-4_4.55x2.6mm_P1.27mm" H 1350 1650 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12884&prodName=TLP291" H 1550 1850 50  0001 L CNN
F 4 "https://hu.farnell.com/toshiba/tlp291-gb-se/optocoupler-phototrans-3-75kv/dp/2524307?st=TLP291" H 1550 1850 50  0001 C CNN "link"
	1    1550 1850
	1    0    0    -1  
$EndComp
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D97B72D
P 1950 1200
F 0 "R?" H 1880 1154 50  0000 R CNN
F 1 "1.9kΩ" H 1880 1245 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1880 1200 50  0001 C CNN
F 3 "~" H 1950 1200 50  0001 C CNN
F 4 "https://hu.farnell.com/vishay/crcw06031k91fkea/res-1k91-1-0-1w-0603-thick-film/dp/2138373" H 1950 1200 50  0001 C CNN "link"
	1    1950 1200
	-1   0    0    1   
$EndComp
Text GLabel 1950 1000 1    50   Input ~ 0
+5V
Text GLabel 2050 1950 2    50   Input ~ 0
GND
Wire Wire Line
	1850 1950 2050 1950
Wire Wire Line
	1950 1750 1850 1750
Wire Wire Line
	1950 1050 1950 1000
Wire Wire Line
	1950 1350 1950 1600
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D9803A6
P 2200 1600
F 0 "R?" V 2407 1600 50  0000 C CNN
F 1 "100Ω" V 2316 1600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2130 1600 50  0001 C CNN
F 3 "~" H 2200 1600 50  0001 C CNN
F 4 "https://hu.farnell.com/panasonic/erj3ekf1000v/res-100r-1-0-1w-0603-thick-film/dp/2303059" V 2200 1600 50  0001 C CNN "link"
	1    2200 1600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1950 1600 2050 1600
Connection ~ 1950 1600
Wire Wire Line
	1950 1600 1950 1750
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D9824F7
P 1050 1350
F 0 "R?" H 980 1304 50  0000 R CNN
F 1 "100Ω" H 980 1395 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 980 1350 50  0001 C CNN
F 3 "~" H 1050 1350 50  0001 C CNN
F 4 "https://hu.farnell.com/panasonic/erj3ekf1000v/res-100r-1-0-1w-0603-thick-film/dp/2303059" H 1050 1350 50  0001 C CNN "link"
	1    1050 1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	1050 1500 1050 1600
Wire Wire Line
	1050 1750 1250 1750
Text GLabel 1050 900  1    50   Input ~ 0
+5V
Wire Wire Line
	1050 900  1050 1050
Text Notes 5850 6750 0    50   ~ 0
Relay\nCoil Connector\nB
$Comp
L RelayTester-rescue:C-Device C?
U 1 1 5D98BDAC
P 750 1350
F 0 "C?" H 865 1396 50  0000 L CNN
F 1 "1μF" H 865 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 788 1200 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2792994.pdf" H 750 1350 50  0001 C CNN
F 4 "https://hu.farnell.com/tdk/c1608x7r1c105k080ac/cap-1-f-16v-10-x7r-0603/dp/1907343" H 750 1350 50  0001 C CNN "link"
	1    750  1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  1500 750  1600
Wire Wire Line
	750  1600 1050 1600
Connection ~ 1050 1600
Wire Wire Line
	1050 1600 1050 1750
Wire Wire Line
	750  1200 750  1050
Wire Wire Line
	750  1050 1050 1050
Connection ~ 1050 1050
Wire Wire Line
	1050 1050 1050 1200
$Comp
L Connector:Conn_01x05_Male J?
U 1 1 5D98DB20
P 900 6300
F 0 "J?" H 1008 6681 50  0000 C CNN
F 1 "Conn_01x05_Male" H 1008 6590 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 900 6300 50  0001 C CNN
F 3 "~" H 900 6300 50  0001 C CNN
	1    900  6300
	1    0    0    -1  
$EndComp
Text Notes 850  6100 2    50   ~ 0
MCLR
Text GLabel 1250 6200 2    50   Input ~ 0
+5V
Wire Wire Line
	1100 6200 1250 6200
Text GLabel 1250 6300 2    50   Input ~ 0
GND
Wire Wire Line
	1250 6300 1100 6300
Text Notes 850  6200 2    50   ~ 0
VDD
Text Notes 850  6300 2    50   ~ 0
Ground
Text Notes 850  6400 2    50   ~ 0
PGD
Text Notes 850  6500 2    50   ~ 0
PGC
Text GLabel 1250 6100 2    50   Input ~ 0
MCLR
Wire Wire Line
	1100 6100 1250 6100
Text GLabel 1250 6400 2    50   Input ~ 0
PGD
Text GLabel 1250 6500 2    50   Input ~ 0
PGC
Wire Wire Line
	1100 6400 1250 6400
Wire Wire Line
	1100 6500 1250 6500
Text Notes 1250 5800 2    50   ~ 0
PICKit4\nProgrammer
Text GLabel 2550 1600 2    50   Input ~ 0
POLE0
Text GLabel 2550 3100 2    50   Input ~ 0
POLE1
Text GLabel 2600 4600 2    50   Input ~ 0
POLE2
Wire Wire Line
	5800 7300 5800 7100
Wire Wire Line
	5900 7300 5900 7100
Wire Wire Line
	6000 7300 6000 7100
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D9A982B
P 1950 2700
F 0 "R?" H 1880 2654 50  0000 R CNN
F 1 "1.9kΩ" H 1880 2745 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1880 2700 50  0001 C CNN
F 3 "~" H 1950 2700 50  0001 C CNN
F 4 "https://hu.farnell.com/vishay/crcw06031k91fkea/res-1k91-1-0-1w-0603-thick-film/dp/2138373" H 1950 2700 50  0001 C CNN "link"
	1    1950 2700
	-1   0    0    1   
$EndComp
Text GLabel 1950 2500 1    50   Input ~ 0
+5V
Text GLabel 2050 3450 2    50   Input ~ 0
GND
Wire Wire Line
	1850 3450 2050 3450
Wire Wire Line
	1950 3250 1850 3250
Wire Wire Line
	1950 2550 1950 2500
Wire Wire Line
	1950 2850 1950 3100
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D9A9837
P 2200 3100
F 0 "R?" V 2407 3100 50  0000 C CNN
F 1 "100Ω" V 2316 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2130 3100 50  0001 C CNN
F 3 "~" H 2200 3100 50  0001 C CNN
F 4 "https://hu.farnell.com/panasonic/erj3ekf1000v/res-100r-1-0-1w-0603-thick-film/dp/2303059" V 2200 3100 50  0001 C CNN "link"
	1    2200 3100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1950 3100 2050 3100
Connection ~ 1950 3100
Wire Wire Line
	1950 3100 1950 3250
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D9A9842
P 1050 2850
F 0 "R?" H 980 2804 50  0000 R CNN
F 1 "100Ω" H 980 2895 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 980 2850 50  0001 C CNN
F 3 "~" H 1050 2850 50  0001 C CNN
F 4 "https://hu.farnell.com/panasonic/erj3ekf1000v/res-100r-1-0-1w-0603-thick-film/dp/2303059" H 1050 2850 50  0001 C CNN "link"
	1    1050 2850
	-1   0    0    1   
$EndComp
Wire Wire Line
	1050 3000 1050 3100
Wire Wire Line
	1050 3250 1250 3250
Text GLabel 1050 2400 1    50   Input ~ 0
+5V
Wire Wire Line
	1050 2400 1050 2550
Wire Wire Line
	750  3000 750  3100
Wire Wire Line
	750  3100 1050 3100
Connection ~ 1050 3100
Wire Wire Line
	1050 3100 1050 3250
Wire Wire Line
	750  2700 750  2550
Wire Wire Line
	750  2550 1050 2550
Connection ~ 1050 2550
Wire Wire Line
	1050 2550 1050 2700
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D9ACB56
P 2000 4200
F 0 "R?" H 1930 4154 50  0000 R CNN
F 1 "1.9kΩ" H 1930 4245 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1930 4200 50  0001 C CNN
F 3 "~" H 2000 4200 50  0001 C CNN
F 4 "https://hu.farnell.com/vishay/crcw06031k91fkea/res-1k91-1-0-1w-0603-thick-film/dp/2138373" H 2000 4200 50  0001 C CNN "link"
	1    2000 4200
	-1   0    0    1   
$EndComp
Text GLabel 2000 4000 1    50   Input ~ 0
+5V
Text GLabel 2100 4950 2    50   Input ~ 0
GND
Wire Wire Line
	1900 4950 2100 4950
Wire Wire Line
	2000 4750 1900 4750
Wire Wire Line
	2000 4050 2000 4000
Wire Wire Line
	2000 4350 2000 4600
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D9ACB62
P 2250 4600
F 0 "R?" V 2457 4600 50  0000 C CNN
F 1 "100Ω" V 2366 4600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2180 4600 50  0001 C CNN
F 3 "~" H 2250 4600 50  0001 C CNN
F 4 "https://hu.farnell.com/panasonic/erj3ekf1000v/res-100r-1-0-1w-0603-thick-film/dp/2303059" V 2250 4600 50  0001 C CNN "link"
	1    2250 4600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2000 4600 2100 4600
Connection ~ 2000 4600
Wire Wire Line
	2000 4600 2000 4750
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D9ACB6D
P 1100 4350
F 0 "R?" H 1030 4304 50  0000 R CNN
F 1 "100Ω" H 1030 4395 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1030 4350 50  0001 C CNN
F 3 "~" H 1100 4350 50  0001 C CNN
F 4 "https://hu.farnell.com/panasonic/erj3ekf1000v/res-100r-1-0-1w-0603-thick-film/dp/2303059" H 1100 4350 50  0001 C CNN "link"
	1    1100 4350
	-1   0    0    1   
$EndComp
Wire Wire Line
	1100 4500 1100 4600
Wire Wire Line
	1100 4750 1300 4750
Text GLabel 1100 3900 1    50   Input ~ 0
+5V
Wire Wire Line
	1100 3900 1100 4050
Wire Wire Line
	800  4500 800  4600
Wire Wire Line
	800  4600 1100 4600
Connection ~ 1100 4600
Wire Wire Line
	1100 4600 1100 4750
Wire Wire Line
	800  4200 800  4050
Wire Wire Line
	800  4050 1100 4050
Connection ~ 1100 4050
Wire Wire Line
	1100 4050 1100 4200
Text GLabel 6000 7300 3    50   Input ~ 0
CONN0
Text GLabel 5900 7300 3    50   Input ~ 0
CONN1
Text GLabel 5800 7300 3    50   Input ~ 0
CONN2
Text GLabel 1100 1950 0    50   Input ~ 0
CONN0
Wire Wire Line
	1100 1950 1250 1950
Text GLabel 1000 3450 0    50   Input ~ 0
CONN1
Wire Wire Line
	1000 3450 1250 3450
Text GLabel 1050 4950 0    50   Input ~ 0
CONN2
Wire Wire Line
	1050 4950 1300 4950
$Comp
L RelayTester-rescue:Barrel_Jack_MountingPin-Connector J?
U 1 1 5D9BB49F
P 950 7150
F 0 "J?" H 1007 7467 50  0000 C CNN
F 1 "PIN" H 1007 7376 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x02_P5.08mm_Drill1.5mm" H 1000 7110 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2645319.pdf" H 1000 7110 50  0001 C CNN
F 4 "https://hu.farnell.com/pro-elec/pe000031/dc-power-jack-connector-4a-chassis/dp/2911014" H 950 7150 50  0001 C CNN "link"
	1    950  7150
	1    0    0    -1  
$EndComp
Text GLabel 1950 7050 2    50   Input ~ 0
+5V
Wire Wire Line
	1250 7050 1400 7050
Text GLabel 1950 7250 2    50   Input ~ 0
GND
Wire Wire Line
	1700 7050 1950 7050
Wire Wire Line
	1250 7250 1950 7250
Text GLabel 8750 2050 2    50   Input ~ 0
RELAY
Wire Wire Line
	8250 2050 8250 2400
Connection ~ 8250 2400
Wire Wire Line
	8650 2050 8750 2050
Wire Wire Line
	8250 2050 8450 2050
$Comp
L RelayTester-rescue:D_Schottky-Device D?
U 1 1 5DA58490
P 10000 1900
F 0 "D?" V 10046 1821 50  0000 R CNN
F 1 "1N5819" V 9955 1821 50  0000 R CNN
F 2 "Diode_THT:D_DO-41_SOD81_P7.62mm_Horizontal" H 10000 1900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2303873.pdf" H 10000 1900 50  0001 C CNN
F 4 "https://hu.farnell.com/on-semiconductor/1n5819/diode-schottky-1a-40v-do-41/dp/1017589?st=1n5819" V 10000 1900 50  0001 C CNN "link"
	1    10000 1900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10000 1650 10000 1750
Wire Wire Line
	10000 2050 10000 2200
$Comp
L RelayTester-rescue:D_Zener-Device D?
U 1 1 5DA65834
P 8250 2850
F 0 "D?" V 8204 2929 50  0000 L CNN
F 1 "1N4733A" V 8295 2929 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P7.62mm_Horizontal" H 8250 2850 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1482758.pdf" H 8250 2850 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/1n4733a/diode-zener-1w-5-1v-do-41/dp/1861447?st=1n4733a" V 8250 2850 50  0001 C CNN "link"
	1    8250 2850
	0    1    1    0   
$EndComp
$Comp
L RelayTester-rescue:D_Schottky-Device D?
U 1 1 5DA66029
P 1550 7050
F 0 "D?" H 1550 6834 50  0000 C CNN
F 1 "1N5819" H 1550 6925 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P7.62mm_Horizontal" H 1550 7050 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2303873.pdf" H 1550 7050 50  0001 C CNN
F 4 "https://hu.farnell.com/on-semiconductor/1n5819/diode-schottky-1a-40v-do-41/dp/1017589?st=1n5819" V 1550 7050 50  0001 C CNN "link"
	1    1550 7050
	-1   0    0    1   
$EndComp
Wire Notes Line
	600  6650 2400 6650
Wire Notes Line
	2400 6650 2400 7750
Wire Notes Line
	2400 7750 600  7750
Wire Notes Line
	600  7750 600  6650
Text Notes 2350 6600 2    50   ~ 0
POWER
Wire Notes Line
	1550 6600 550  6600
Wire Notes Line
	550  6600 550  5900
Wire Notes Line
	550  5900 1550 5900
Wire Notes Line
	1550 5900 1550 6600
Wire Notes Line
	3050 5350 3050 600 
Wire Notes Line
	3050 600  650  600 
Wire Notes Line
	650  600  650  5350
Wire Notes Line
	650  5350 3050 5350
Text Notes 3050 550  2    50   ~ 0
Continuity
Wire Notes Line
	10950 750  10950 3350
Text Notes 10850 650  2    50   ~ 0
Relay Control
$Comp
L RelayTester-rescue:TLP291-Isolator U?
U 1 1 5DABB65F
P 1550 3350
F 0 "U?" H 1550 3675 50  0000 C CNN
F 1 "TLP291" H 1550 3584 50  0000 C CNN
F 2 "Package_SO:SOIC-4_4.55x2.6mm_P1.27mm" H 1350 3150 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12884&prodName=TLP291" H 1550 3350 50  0001 L CNN
F 4 "https://hu.farnell.com/toshiba/tlp291-gb-se/optocoupler-phototrans-3-75kv/dp/2524307?st=TLP291" H 1550 3350 50  0001 C CNN "link"
	1    1550 3350
	1    0    0    -1  
$EndComp
$Comp
L RelayTester-rescue:TLP291-Isolator U?
U 1 1 5DABC270
P 1600 4850
F 0 "U?" H 1600 5175 50  0000 C CNN
F 1 "TLP291" H 1600 5084 50  0000 C CNN
F 2 "Package_SO:SOIC-4_4.55x2.6mm_P1.27mm" H 1400 4650 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12884&prodName=TLP291" H 1600 4850 50  0001 L CNN
F 4 "https://hu.farnell.com/toshiba/tlp291-gb-se/optocoupler-phototrans-3-75kv/dp/2524307?st=TLP291" H 1600 4850 50  0001 C CNN "link"
	1    1600 4850
	1    0    0    -1  
$EndComp
Wire Notes Line
	3250 3900 3250 800 
Wire Notes Line
	5700 3900 3250 3900
Wire Notes Line
	5700 800  5700 3900
Wire Wire Line
	4450 3550 4450 3800
Connection ~ 4450 3550
Wire Wire Line
	4300 3550 4450 3550
Wire Wire Line
	4300 3750 4300 3550
Wire Wire Line
	4150 3300 4350 3300
Wire Wire Line
	4150 3350 4150 3300
Text GLabel 4350 3300 2    50   Input ~ 0
+5V
Wire Wire Line
	4050 3800 4450 3800
Wire Wire Line
	3500 3550 3550 3550
Connection ~ 3500 3550
Wire Wire Line
	3500 3800 3850 3800
Wire Wire Line
	3500 3550 3500 3800
Wire Wire Line
	3450 3550 3500 3550
Wire Wire Line
	4450 3550 4500 3550
Wire Wire Line
	4150 3750 4300 3750
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5DB03EA4
P 3700 3550
F 0 "R?" V 3493 3550 50  0000 C CNN
F 1 "2kΩ" V 3584 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3630 3550 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2563624.pdf" H 3700 3550 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x2001ftl/res-2k-1-0-1w-0603-thick-film/dp/2447319?st=smd%20resistor%202k" V 3700 3550 50  0001 C CNN "link"
	1    3700 3550
	0    1    1    0   
$EndComp
$Comp
L RelayTester-rescue:BC847-Transistor_BJT Q?
U 1 1 5DB03E9D
P 4050 3550
F 0 "Q?" H 4241 3596 50  0000 L CNN
F 1 "BC847" H 4241 3505 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4250 3475 50  0001 L CIN
F 3 "http://www.farnell.com/datasheets/1911603.pdf" H 4050 3550 50  0001 L CNN
F 4 "https://hu.farnell.com/on-semiconductor/bc847alt1g/transistor-npn-sot-23/dp/1459037?st=bc847" H 4050 3550 50  0001 C CNN "link"
	1    4050 3550
	1    0    0    -1  
$EndComp
Text GLabel 3450 3550 0    50   Input ~ 0
ST4
Wire Wire Line
	5100 3550 5350 3550
Text GLabel 5350 3550 2    50   Input ~ 0
GND
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5DB03E8D
P 4650 3550
F 0 "R?" H 4720 3596 50  0000 L CNN
F 1 "250Ω" H 4720 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4580 3550 50  0001 C CNN
F 3 "" H 4650 3550 50  0001 C CNN
F 4 "https://hu.farnell.com/vishay/crcw0603249rfkea/res-249r-1-0-1w-0603-thick-film/dp/2141316" H 4650 3550 50  0001 C CNN "link"
	1    4650 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	4450 2950 4450 3200
Connection ~ 4450 2950
Wire Wire Line
	4300 2950 4450 2950
Wire Wire Line
	4300 3150 4300 2950
Wire Wire Line
	4150 2700 4350 2700
Wire Wire Line
	4150 2750 4150 2700
Text GLabel 4350 2700 2    50   Input ~ 0
+5V
Wire Wire Line
	4050 3200 4450 3200
Wire Wire Line
	3500 2950 3550 2950
Connection ~ 3500 2950
Wire Wire Line
	3500 3200 3850 3200
Wire Wire Line
	3500 2950 3500 3200
Wire Wire Line
	3450 2950 3500 2950
Wire Wire Line
	4450 2950 4500 2950
Wire Wire Line
	4150 3150 4300 3150
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5DAFB05E
P 3700 2950
F 0 "R?" V 3493 2950 50  0000 C CNN
F 1 "2kΩ" V 3584 2950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3630 2950 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2563624.pdf" H 3700 2950 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x2001ftl/res-2k-1-0-1w-0603-thick-film/dp/2447319?st=smd%20resistor%202k" V 3700 2950 50  0001 C CNN "link"
	1    3700 2950
	0    1    1    0   
$EndComp
$Comp
L RelayTester-rescue:BC847-Transistor_BJT Q?
U 1 1 5DAFB057
P 4050 2950
F 0 "Q?" H 4241 2996 50  0000 L CNN
F 1 "BC847" H 4241 2905 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4250 2875 50  0001 L CIN
F 3 "http://www.farnell.com/datasheets/1911603.pdf" H 4050 2950 50  0001 L CNN
F 4 "https://hu.farnell.com/on-semiconductor/bc847alt1g/transistor-npn-sot-23/dp/1459037?st=bc847" H 4050 2950 50  0001 C CNN "link"
	1    4050 2950
	1    0    0    -1  
$EndComp
Text GLabel 3450 2950 0    50   Input ~ 0
ST3
Wire Wire Line
	5100 2950 5350 2950
Text GLabel 5350 2950 2    50   Input ~ 0
GND
Wire Wire Line
	4450 2350 4450 2600
Connection ~ 4450 2350
Wire Wire Line
	4300 2350 4450 2350
Wire Wire Line
	4300 2550 4300 2350
Wire Wire Line
	4150 2100 4350 2100
Wire Wire Line
	4150 2150 4150 2100
Text GLabel 4350 2100 2    50   Input ~ 0
+5V
Wire Wire Line
	4050 2600 4450 2600
Wire Wire Line
	3500 2350 3550 2350
Connection ~ 3500 2350
Wire Wire Line
	3500 2600 3850 2600
Wire Wire Line
	3500 2350 3500 2600
Wire Wire Line
	3450 2350 3500 2350
Wire Wire Line
	4450 2350 4500 2350
Wire Wire Line
	4150 2550 4300 2550
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5DAF4384
P 3700 2350
F 0 "R?" V 3493 2350 50  0000 C CNN
F 1 "2kΩ" V 3584 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3630 2350 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2563624.pdf" H 3700 2350 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x2001ftl/res-2k-1-0-1w-0603-thick-film/dp/2447319?st=smd%20resistor%202k" V 3700 2350 50  0001 C CNN "link"
	1    3700 2350
	0    1    1    0   
$EndComp
Text GLabel 3450 2350 0    50   Input ~ 0
ST2
Wire Wire Line
	5100 2350 5350 2350
Text GLabel 5350 2350 2    50   Input ~ 0
GND
Wire Wire Line
	4450 1750 4450 2000
Connection ~ 4450 1750
Wire Wire Line
	4300 1750 4450 1750
Wire Wire Line
	4300 1950 4300 1750
Wire Wire Line
	4150 1500 4350 1500
Wire Wire Line
	4150 1550 4150 1500
Text GLabel 4350 1500 2    50   Input ~ 0
+5V
Wire Wire Line
	4050 2000 4450 2000
Wire Wire Line
	3500 1750 3550 1750
Connection ~ 3500 1750
Wire Wire Line
	3500 2000 3850 2000
Wire Wire Line
	3500 1750 3500 2000
Wire Wire Line
	3450 1750 3500 1750
Wire Wire Line
	4450 1750 4500 1750
Wire Wire Line
	4150 1950 4300 1950
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5DAECE7B
P 3700 1750
F 0 "R?" V 3493 1750 50  0000 C CNN
F 1 "2kΩ" V 3584 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3630 1750 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2563624.pdf" H 3700 1750 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x2001ftl/res-2k-1-0-1w-0603-thick-film/dp/2447319?st=smd%20resistor%202k" V 3700 1750 50  0001 C CNN "link"
	1    3700 1750
	0    1    1    0   
$EndComp
$Comp
L RelayTester-rescue:BC847-Transistor_BJT Q?
U 1 1 5DAECE74
P 4050 1750
F 0 "Q?" H 4241 1796 50  0000 L CNN
F 1 "BC847" H 4241 1705 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4250 1675 50  0001 L CIN
F 3 "http://www.farnell.com/datasheets/1911603.pdf" H 4050 1750 50  0001 L CNN
F 4 "https://hu.farnell.com/on-semiconductor/bc847alt1g/transistor-npn-sot-23/dp/1459037?st=bc847" H 4050 1750 50  0001 C CNN "link"
	1    4050 1750
	1    0    0    -1  
$EndComp
Text GLabel 3450 1750 0    50   Input ~ 0
ST1
Wire Wire Line
	5100 1750 5350 1750
Text GLabel 5350 1750 2    50   Input ~ 0
GND
Wire Wire Line
	4450 1150 4450 1400
Connection ~ 4450 1150
Wire Wire Line
	4300 1150 4450 1150
Wire Wire Line
	4300 1350 4300 1150
Wire Wire Line
	4150 900  4350 900 
Wire Wire Line
	4150 950  4150 900 
Text GLabel 4350 900  2    50   Input ~ 0
+5V
Wire Wire Line
	4050 1400 4450 1400
Wire Wire Line
	3500 1150 3550 1150
Connection ~ 3500 1150
Wire Wire Line
	3500 1400 3850 1400
Wire Wire Line
	3500 1150 3500 1400
Wire Wire Line
	3450 1150 3500 1150
Wire Wire Line
	4450 1150 4500 1150
Wire Wire Line
	4150 1350 4300 1350
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5DAC1C17
P 3700 1150
F 0 "R?" V 3493 1150 50  0000 C CNN
F 1 "2kΩ" V 3584 1150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3630 1150 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2563624.pdf" H 3700 1150 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x2001ftl/res-2k-1-0-1w-0603-thick-film/dp/2447319?st=smd%20resistor%202k" V 3700 1150 50  0001 C CNN "link"
	1    3700 1150
	0    1    1    0   
$EndComp
$Comp
L RelayTester-rescue:BC847-Transistor_BJT Q?
U 1 1 5DAC1027
P 4050 1150
F 0 "Q?" H 4241 1196 50  0000 L CNN
F 1 "BC847" H 4241 1105 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4250 1075 50  0001 L CIN
F 3 "http://www.farnell.com/datasheets/1911603.pdf" H 4050 1150 50  0001 L CNN
F 4 "https://hu.farnell.com/on-semiconductor/bc847alt1g/transistor-npn-sot-23/dp/1459037?st=bc847" H 4050 1150 50  0001 C CNN "link"
	1    4050 1150
	1    0    0    -1  
$EndComp
Text Notes 5650 750  2    50   ~ 0
Status
Wire Notes Line
	3250 800  5700 800 
Text GLabel 3450 1150 0    50   Input ~ 0
ST0
Wire Wire Line
	5100 1150 5350 1150
Text GLabel 5350 1150 2    50   Input ~ 0
GND
$Comp
L RelayTester-rescue:LED-Device D?
U 1 1 5DA035D0
P 4950 1150
F 0 "D?" H 4943 895 50  0000 C CNN
F 1 "LED" H 4943 986 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4950 1150 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1911343.pdf" H 4950 1150 50  0001 C CNN
F 4 "https://hu.farnell.com/wurth-elektronik/150060bs75000/led-0603-blue-145mcd-470nm/dp/2322069?st=smd%20led" H 4950 1150 50  0001 C CNN "link"
	1    4950 1150
	-1   0    0    1   
$EndComp
Wire Wire Line
	10200 5500 10300 5500
Wire Wire Line
	10200 5400 10300 5400
Wire Wire Line
	10200 5300 10300 5300
Wire Wire Line
	10200 5200 10300 5200
Wire Wire Line
	10200 5100 10300 5100
Text GLabel 10300 5500 2    50   Input ~ 0
ST4
Text GLabel 10300 5400 2    50   Input ~ 0
ST3
Text GLabel 10300 5300 2    50   Input ~ 0
ST2
Text GLabel 10300 5200 2    50   Input ~ 0
ST1
Text GLabel 10300 5100 2    50   Input ~ 0
ST0
Wire Wire Line
	8900 3850 8900 3950
Wire Wire Line
	10300 5000 10200 5000
Text GLabel 10300 5000 2    50   Input ~ 0
RELAY
Wire Wire Line
	7000 5300 7600 5300
Wire Wire Line
	7000 5200 7600 5200
Wire Wire Line
	7000 5100 7600 5100
Text GLabel 7000 5300 0    50   Input ~ 0
POLE2
Text GLabel 7000 5200 0    50   Input ~ 0
POLE1
Text GLabel 7000 5100 0    50   Input ~ 0
POLE0
Wire Wire Line
	8150 4350 8400 4350
Text GLabel 8150 4350 0    50   Input ~ 0
GND
Wire Wire Line
	8700 4350 8900 4350
$Comp
L RelayTester-rescue:C-Device C?
U 1 1 5D9D9AAB
P 8550 4350
F 0 "C?" V 8298 4350 50  0000 C CNN
F 1 "10μF" V 8389 4350 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8588 4200 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2340522.pdf" H 8550 4350 50  0001 C CNN
F 4 "https://hu.farnell.com/samsung-electro-mechanics/cl10a106mq8nnnc/cap-10uf-6-3v-mlcc-0603/dp/3013391" V 8550 4350 50  0001 C CNN "link"
	1    8550 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	9550 4350 9350 4350
Text GLabel 9550 4350 2    50   Input ~ 0
GND
Wire Wire Line
	8900 4350 8900 4150
Connection ~ 8900 4350
Wire Wire Line
	9050 4350 8900 4350
$Comp
L RelayTester-rescue:C-Device C?
U 1 1 5D9D457E
P 9200 4350
F 0 "C?" V 9452 4350 50  0000 C CNN
F 1 "100nF" V 9361 4350 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9238 4200 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2079171.pdf" H 9200 4350 50  0001 C CNN
F 4 "https://hu.farnell.com/kemet/c0603c104m4ractu/cap-0-1-f-16v-20-x7r-0603/dp/2581045" V 9200 4350 50  0001 C CNN "link"
	1    9200 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8900 4700 8900 4350
Text GLabel 8900 3850 1    50   Input ~ 0
+5V
Text GLabel 8900 6300 3    50   Input ~ 0
GND
Wire Wire Line
	10300 5600 10200 5600
Text GLabel 10300 5600 2    50   Input ~ 0
PGC
Wire Wire Line
	10200 5700 10300 5700
Text GLabel 10300 5700 2    50   Input ~ 0
PGD
Wire Wire Line
	7350 5000 7600 5000
Text GLabel 7350 5000 0    50   Input ~ 0
MCLR
$Comp
L MCU_Microchip_PIC18:PIC18F1320-SO U?
U 1 1 5D95C625
P 8900 5400
F 0 "U?" H 8900 6281 50  0000 C CNN
F 1 "PIC18F1320-SO" H 8900 6190 50  0000 C CNN
F 2 "Package_SO:SOIC-18W_7.5x11.6mm_P1.27mm" H 8900 5400 50  0001 C CIN
F 3 "http://www.farnell.com/datasheets/2243472.pdf" H 8750 5950 50  0001 C CNN
F 4 "https://hu.farnell.com/microchip/pic18f1320-i-so/mcu-8bit-pic18-40mhz-soic-18/dp/9762027?st=pic18f1320" H 8900 5400 50  0001 C CNN "link"
	1    8900 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 6300 8900 6000
$Comp
L RelayTester-rescue:LED-Device D?
U 1 1 5DB7D619
P 4950 1750
F 0 "D?" H 4943 1495 50  0000 C CNN
F 1 "LED" H 4943 1586 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4950 1750 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1911343.pdf" H 4950 1750 50  0001 C CNN
F 4 "https://hu.farnell.com/wurth-elektronik/150060bs75000/led-0603-blue-145mcd-470nm/dp/2322069?st=smd%20led" H 4950 1750 50  0001 C CNN "link"
	1    4950 1750
	-1   0    0    1   
$EndComp
$Comp
L RelayTester-rescue:LED-Device D?
U 1 1 5DB7DD1C
P 4950 2350
F 0 "D?" H 4943 2095 50  0000 C CNN
F 1 "LED" H 4943 2186 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4950 2350 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1911343.pdf" H 4950 2350 50  0001 C CNN
F 4 "https://hu.farnell.com/wurth-elektronik/150060bs75000/led-0603-blue-145mcd-470nm/dp/2322069?st=smd%20led" H 4950 2350 50  0001 C CNN "link"
	1    4950 2350
	-1   0    0    1   
$EndComp
$Comp
L RelayTester-rescue:LED-Device D?
U 1 1 5DB7E2E1
P 4950 2950
F 0 "D?" H 4943 2695 50  0000 C CNN
F 1 "LED" H 4943 2786 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4950 2950 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1911343.pdf" H 4950 2950 50  0001 C CNN
F 4 "https://hu.farnell.com/wurth-elektronik/150060bs75000/led-0603-blue-145mcd-470nm/dp/2322069?st=smd%20led" H 4950 2950 50  0001 C CNN "link"
	1    4950 2950
	-1   0    0    1   
$EndComp
$Comp
L RelayTester-rescue:LED-Device D?
U 1 1 5DB7E76E
P 4950 3550
F 0 "D?" H 4943 3295 50  0000 C CNN
F 1 "LED" H 4943 3386 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4950 3550 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1911343.pdf" H 4950 3550 50  0001 C CNN
F 4 "https://hu.farnell.com/wurth-elektronik/150060bs75000/led-0603-blue-145mcd-470nm/dp/2322069?st=smd%20led" H 4950 3550 50  0001 C CNN "link"
	1    4950 3550
	-1   0    0    1   
$EndComp
$Comp
L RelayTester-rescue:Jumper_NO_Small-Device JP?
U 1 1 5DB80449
P 8900 4050
F 0 "JP?" H 8900 4235 50  0000 C CNN
F 1 "JMP" H 8900 4144 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 8900 4050 50  0001 C CNN
F 3 "~" H 8900 4050 50  0001 C CNN
	1    8900 4050
	0    1    1    0   
$EndComp
$Comp
L RelayTester-rescue:Jumper_NO_Small-Device JP?
U 1 1 5DB80E9E
P 2450 1600
F 0 "JP?" H 2450 1785 50  0000 C CNN
F 1 "JMP" H 2450 1694 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 2450 1600 50  0001 C CNN
F 3 "~" H 2450 1600 50  0001 C CNN
	1    2450 1600
	-1   0    0    1   
$EndComp
$Comp
L RelayTester-rescue:Jumper_NO_Small-Device JP?
U 1 1 5DB830F2
P 2450 3100
F 0 "JP?" H 2450 3285 50  0000 C CNN
F 1 "JMP" H 2450 3194 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 2450 3100 50  0001 C CNN
F 3 "~" H 2450 3100 50  0001 C CNN
	1    2450 3100
	-1   0    0    1   
$EndComp
$Comp
L RelayTester-rescue:Jumper_NO_Small-Device JP?
U 1 1 5DB83804
P 2500 4600
F 0 "JP?" H 2500 4785 50  0000 C CNN
F 1 "JMP" H 2500 4694 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 2500 4600 50  0001 C CNN
F 3 "~" H 2500 4600 50  0001 C CNN
	1    2500 4600
	-1   0    0    1   
$EndComp
$Comp
L RelayTester-rescue:RJ45_LED-Connector J?
U 1 1 5DB86D3E
P 9100 1350
F 0 "J?" H 9100 2017 50  0000 C CNN
F 1 "RJ45_LED" H 9100 1926 50  0000 C CNN
F 2 "Connector_RJ:RJ45_Amphenol_RJHSE538X" V 9100 1375 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1912418.pdf" V 9100 1375 50  0001 C CNN
F 4 "https://hu.farnell.com/amphenol-icc-commercial-products/rjhse-5380/jack-rj45-shield-1port/dp/1860580?st=rjhse538" H 9100 1350 50  0001 C CNN "link"
	1    9100 1350
	1    0    0    -1  
$EndComp
Text GLabel 8250 1650 0    50   Input ~ 0
GND
Text GLabel 8250 1050 0    50   Input ~ 0
GND
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5DB97FB5
P 8450 1650
F 0 "R?" V 8243 1650 50  0000 C CNN
F 1 "2kΩ" V 8334 1650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8380 1650 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2563624.pdf" H 8450 1650 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x2001ftl/res-2k-1-0-1w-0603-thick-film/dp/2447319?st=smd%20resistor%202k" V 8450 1650 50  0001 C CNN "link"
	1    8450 1650
	0    1    1    0   
$EndComp
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5DB98888
P 8450 1050
F 0 "R?" V 8243 1050 50  0000 C CNN
F 1 "2kΩ" V 8334 1050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8380 1050 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2563624.pdf" H 8450 1050 50  0001 C CNN
F 4 "https://hu.farnell.com/multicomp/mcwr06x2001ftl/res-2k-1-0-1w-0603-thick-film/dp/2447319?st=smd%20resistor%202k" V 8450 1050 50  0001 C CNN "link"
	1    8450 1050
	0    1    1    0   
$EndComp
Wire Wire Line
	8600 1050 8700 1050
Wire Wire Line
	8250 1050 8300 1050
Wire Wire Line
	8250 1650 8300 1650
Wire Wire Line
	8600 1650 8700 1650
Text GLabel 7400 5400 0    50   Input ~ 0
ETHLED0
Text GLabel 7400 5500 0    50   Input ~ 0
ETHLED1
Wire Wire Line
	7400 5400 7600 5400
Wire Wire Line
	7400 5500 7600 5500
Text GLabel 8250 1500 0    50   Input ~ 0
ETHLED1
Wire Wire Line
	8250 1500 8600 1500
Wire Wire Line
	8600 1500 8600 1550
Wire Wire Line
	8600 1550 8700 1550
Text GLabel 8250 900  0    50   Input ~ 0
ETHLED0
Wire Wire Line
	8250 900  8600 900 
Wire Wire Line
	8600 900  8600 950 
Wire Wire Line
	8600 950  8700 950 
Wire Notes Line
	7800 750  7800 3350
Wire Notes Line
	7800 3350 10950 3350
Wire Notes Line
	7800 750  10950 750 
Wire Wire Line
	9850 1550 10000 1550
Connection ~ 10000 1550
Wire Wire Line
	9650 1250 9650 1350
Wire Wire Line
	9500 1350 9650 1350
Connection ~ 9650 1350
Wire Wire Line
	9500 1250 9650 1250
Wire Wire Line
	9650 1650 10000 1650
Connection ~ 10000 1650
NoConn ~ 9500 1550
NoConn ~ 9500 1650
NoConn ~ 7600 5600
NoConn ~ 7600 5700
$Comp
L RelayTester-rescue:C-Device C?
U 1 1 5DC70B9A
P 750 2850
F 0 "C?" H 865 2896 50  0000 L CNN
F 1 "1μF" H 865 2805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 788 2700 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2792994.pdf" H 750 2850 50  0001 C CNN
F 4 "https://hu.farnell.com/tdk/c1608x7r1c105k080ac/cap-1-f-16v-10-x7r-0603/dp/1907343" H 750 2850 50  0001 C CNN "link"
	1    750  2850
	1    0    0    -1  
$EndComp
$Comp
L RelayTester-rescue:C-Device C?
U 1 1 5DC710AB
P 800 4350
F 0 "C?" H 915 4396 50  0000 L CNN
F 1 "1μF" H 915 4305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 838 4200 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2792994.pdf" H 800 4350 50  0001 C CNN
F 4 "https://hu.farnell.com/tdk/c1608x7r1c105k080ac/cap-1-f-16v-10-x7r-0603/dp/1907343" H 800 4350 50  0001 C CNN "link"
	1    800  4350
	1    0    0    -1  
$EndComp
$Comp
L RelayTester-rescue:BC847-Transistor_BJT Q?
U 1 1 5DAF437D
P 4050 2350
F 0 "Q?" H 4241 2396 50  0000 L CNN
F 1 "BC847" H 4241 2305 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4250 2275 50  0001 L CIN
F 3 "http://www.farnell.com/datasheets/1911603.pdf" H 4050 2350 50  0001 L CNN
F 4 "https://hu.farnell.com/on-semiconductor/bc847alt1g/transistor-npn-sot-23/dp/1459037?st=bc847" H 4050 2350 50  0001 C CNN "link"
	1    4050 2350
	1    0    0    -1  
$EndComp
$Comp
L RelayTester-rescue:Jumper_NO_Small-Device JP?
U 1 1 5DC72BF7
P 3950 1400
F 0 "JP?" H 3950 1585 50  0000 C CNN
F 1 "JMP" H 3950 1494 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3950 1400 50  0001 C CNN
F 3 "~" H 3950 1400 50  0001 C CNN
	1    3950 1400
	-1   0    0    1   
$EndComp
$Comp
L RelayTester-rescue:Jumper_NO_Small-Device JP?
U 1 1 5DC72F1A
P 3950 2000
F 0 "JP?" H 3950 2185 50  0000 C CNN
F 1 "JMP" H 3950 2094 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3950 2000 50  0001 C CNN
F 3 "~" H 3950 2000 50  0001 C CNN
	1    3950 2000
	-1   0    0    1   
$EndComp
$Comp
L RelayTester-rescue:Jumper_NO_Small-Device JP?
U 1 1 5DC732C7
P 3950 2600
F 0 "JP?" H 3950 2785 50  0000 C CNN
F 1 "JMP" H 3950 2694 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3950 2600 50  0001 C CNN
F 3 "~" H 3950 2600 50  0001 C CNN
	1    3950 2600
	-1   0    0    1   
$EndComp
$Comp
L RelayTester-rescue:Jumper_NO_Small-Device JP?
U 1 1 5DC7372E
P 3950 3200
F 0 "JP?" H 3950 3385 50  0000 C CNN
F 1 "JMP" H 3950 3294 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3950 3200 50  0001 C CNN
F 3 "~" H 3950 3200 50  0001 C CNN
	1    3950 3200
	-1   0    0    1   
$EndComp
$Comp
L RelayTester-rescue:Jumper_NO_Small-Device JP?
U 1 1 5DC73CF1
P 3950 3800
F 0 "JP?" H 3950 3985 50  0000 C CNN
F 1 "JMP" H 3950 3894 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 3950 3800 50  0001 C CNN
F 3 "~" H 3950 3800 50  0001 C CNN
	1    3950 3800
	-1   0    0    1   
$EndComp
NoConn ~ 950  7450
Wire Wire Line
	9850 950  9500 950 
Wire Wire Line
	9850 950  9850 1550
NoConn ~ 9500 1150
Wire Wire Line
	9650 1350 9650 1650
Wire Wire Line
	9650 1250 9650 1050
Wire Wire Line
	9650 1050 9500 1050
Connection ~ 9650 1250
NoConn ~ 9500 1450
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D98315A
P 4650 2950
F 0 "R?" H 4720 2996 50  0000 L CNN
F 1 "250Ω" H 4720 2905 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4580 2950 50  0001 C CNN
F 3 "" H 4650 2950 50  0001 C CNN
F 4 "https://hu.farnell.com/vishay/crcw0603249rfkea/res-249r-1-0-1w-0603-thick-film/dp/2141316" H 4650 2950 50  0001 C CNN "link"
	1    4650 2950
	0    1    1    0   
$EndComp
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D9831FC
P 4650 2350
F 0 "R?" H 4720 2396 50  0000 L CNN
F 1 "250Ω" H 4720 2305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4580 2350 50  0001 C CNN
F 3 "" H 4650 2350 50  0001 C CNN
F 4 "https://hu.farnell.com/vishay/crcw0603249rfkea/res-249r-1-0-1w-0603-thick-film/dp/2141316" H 4650 2350 50  0001 C CNN "link"
	1    4650 2350
	0    1    1    0   
$EndComp
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D98329A
P 4650 1750
F 0 "R?" H 4720 1796 50  0000 L CNN
F 1 "250Ω" H 4720 1705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4580 1750 50  0001 C CNN
F 3 "" H 4650 1750 50  0001 C CNN
F 4 "https://hu.farnell.com/vishay/crcw0603249rfkea/res-249r-1-0-1w-0603-thick-film/dp/2141316" H 4650 1750 50  0001 C CNN "link"
	1    4650 1750
	0    1    1    0   
$EndComp
$Comp
L RelayTester-rescue:R-Device R?
U 1 1 5D98333E
P 4650 1150
F 0 "R?" H 4720 1196 50  0000 L CNN
F 1 "250Ω" H 4720 1105 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4580 1150 50  0001 C CNN
F 3 "" H 4650 1150 50  0001 C CNN
F 4 "https://hu.farnell.com/vishay/crcw0603249rfkea/res-249r-1-0-1w-0603-thick-film/dp/2141316" H 4650 1150 50  0001 C CNN "link"
	1    4650 1150
	0    1    1    0   
$EndComp
$EndSCHEMATC
